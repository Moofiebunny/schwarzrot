var openpgp = window.openpgp;

$(document).ready(function () {
    $('.toggle').click(function () {
        $(this).children('i').toggleClass('fa-pencil');
        $('.form').stop().animate({
            height: "toggle",
            'padding-top': 'toggle',
            'padding-bottom': 'toggle',
            opacity: "toggle"
        }, "slow");
        $('.toggle').toggleClass('login');
        $('.tooltip').toggleClass('register');
        if ($('.tooltip').hasClass('register')) {
            $('.tooltip').text('Registrieren');
        } else {
            $('.tooltip').text('Schlüssel importieren');
        }
    });

    $('input').on('input', function() {
        validate();
    });

    $('input.register').click(function (e) {
        e.preventDefault();

        validate(function(working) {
            if(working) {
                swal({
                    title: "Erstelle PGP-Schlüssel...",
                    text: 'Zunächst werden lokal zwei PGP-Schlüssel generiert, ein privater und ein öffentlicher. ' +
                    'Dafür bitten wir um etwas Geduld, das kann einige Minuten dauern. Der öffentliche Schlüssel ' +
                    'wird dann zu unseren Servern übertragen, der private wird im Browser und damit auf deinem ' +
                    'PC gespeichert. Für eine zusätzliche Sicherheit wird er mit dem festgelegten Passphrase ' +
                    'verschlüsselt. Für weitere Informationen über das PGP-verfahren klicke ' +
                    '<a href="https://de.wikipedia.org/wiki/Pretty_Good_Privacy">hier</a>.',
                    showConfirmButton: false,
                    html: true,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    type: "info"
                });

                openpgp.initWorker({ path: '/js/openpgp.worker.min.js' });
                openpgp.config.aead_protect = true;

                var username = $('.register .username').val(),
                    passphrase = $('.register .passphrase').val();

                var options = {
                    userIds: [{ name: username, email: username + "@schwarzrot.cc" }],
                    numBits: 4096,
                    passphrase: passphrase
                };

                openpgp.generateKey(options).then(function(key) {
                    console.log('=============== BEGIN PGP PRIVATE KEY BLOCK ===============');
                    console.log(key.privateKeyArmored);
                    console.log('');
                    console.log('');
                    console.log('=============== BEGIN PGP PUBLIC KEY BLOCK ===============');
                    console.log(key.publicKeyArmored);
                    swal.close();
                });
            } else {
                console.log("asdfasdf");
                $('.form.register .fakesubmit').click();
            }
        });
    });

    $('button.key').click(function () {
    });
});

function validate(callback) {
    var username = $('.register .username');
    var passphrase = $('.register .passphrase');
    var passphrase2 = $('.register .passphrase-repeat');
    var inviteCode = $('.register .invitecode');

    var working = true;

    if(username.val().length == 0) {

    if(passphrase2.val() != passphrase.val()) {
        passphrase2.get(0).setCustomValidity("Deine Passphrases stimmen nicht überein!");
        working = false;
    } else passphrase2.get(0).setCustomValidity("");

    if(inviteCode.val().length == 0) {
        inviteCode.get(0).setCustomValidity("Bitte besorge dir einen Invite-Code von einem/r GenossIn!");
        working = false;
    } else inviteCode.get(0).setCustomValidity("");

    $.post("/api/register/checkfields", { "username": username.val(), "invitecode": inviteCode.val() }, function(data) {
        console.log(data);

        if(data.invitecode != true) {
            inviteCode.get(0).setCustomValidity("Dein Invite-Code funktioniert nicht!");
            working = false;
        }
        else inviteCode.get(0).setCustomValidity("");

        if(data.username != true) {
            username.get(0).setCustomValidity("Dein Benutzername ist schon vergeben oder ungültig!");
            working = false;
        }
        else username.get(0).setCustomValidity("");

        if(callback) callback(working);
    });
}

function generatePgpKey() {

}