var db = require('./database');

db.inviteUser(process.argv[2], function(err, invite) {
    if(err) console.log(err);

    console.log(invite.key);
    
    db.close(function() {
        console.log('Database connection closed!');
    });
});