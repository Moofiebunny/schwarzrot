var db = require('./database');

db.invalidateInviteCode(process.argv[2], function(err, ok) {
    if(err) console.log(err);
    if(ok) console.log('Removed!');
    else console.log('Did not work.');

    db.close(function() {
        console.log('Database connection closed!');
    });
});