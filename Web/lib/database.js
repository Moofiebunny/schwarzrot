var _           = require('./functions');
var dbconf      = require('../config').mongodb;
var cryptoconf  = require('../config').encryption;
var crypto      = require('crypto');
var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

console.log('Connecting to: ' + _.buildMongoString(dbconf.host, dbconf.port, dbconf.database, dbconf.username, dbconf.password));
mongoose.connect(_.buildMongoString(dbconf.host, dbconf.port, dbconf.database, dbconf.username, dbconf.password));

var schemas = {
    user: new Schema({
        userName: String,
        publicKey: String
    }),
    invites: new Schema({
        inviteCode: String,
        user: {type: Schema.Types.ObjectId, ref: 'user'}
    })
};

var models = {
    User: mongoose.model('users', schemas.user),
    Invite: mongoose.model('invite', schemas.invites)
};


module.exports = exports = {
    inviteUser: function (username, callback) {
        var key = (Math.random().toString(36).slice(-16) + Math.random().toString(36).slice(-16)).split('.').join("");

        this.checkInviteCode(key, function(working) {
            if(working)
                this.inviteUser(username, callback);
            else {
                var cipher = crypto.createCipher(cryptoconf.algorithm, cryptoconf.key);
                var encrypted = cipher.update(key, 'utf8', 'hex') + cipher.final('hex');
                
                exports.findUserByUsername(username, function(err, user) {
                    if(err)
                        callback(err, null);
                    else {
                        (new models.Invite({
                            inviteCode: encrypted,
                            user:       user._id
                        })).save(function(err, invite) {
                            if(err)
                                callback(err, null);
                            else {
                                callback(null, {
                                    key: key,
                                    encrypted: encrypted
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    checkInviteCode: function (key, callback) {
        var cipher = crypto.createCipher(cryptoconf.algorithm, cryptoconf.key);
        var encrypted = cipher.update(key, 'utf8', 'hex') + cipher.final('hex');

        models.Invite.find({ inviteCode: encrypted }, '_id', function(err, code) {
            if(err)
                callback(err, null);
            else if(code.length == 0)
                callback(null, false);
            else callback(null, true);
        })
    },
    invalidateInviteCode: function (key, callback) {
        exports.checkInviteCode(key, function(err, working) {
            if(err) callback(err, false);
            else {
                if(working) {
                    var cipher = crypto.createCipher(cryptoconf.algorithm, cryptoconf.key);
                    var encrypted = cipher.update(key, 'utf8', 'hex') + cipher.final('hex');

                    models.Invite.remove({ inviteCode: encrypted }, function(err) {
                        if(err)
                            callback(err, false);
                        else
                            callback(null, true);
                    });
                } else callback(null, false);
            }
        });
    },
    findUserByUsername: function (username, callback) {
        models.User.findOne({ userName: username }, '_id userName realName publicKey', function(err, user) {
            if(err)
                callback(err, null);
            else
                callback(null, user);
        });
    },
    checkIfUsernameExists: function(username, callback) {
        models.User.find({ userName: username }, '_id', function(err, users) {
            if(err)
                callback(err, null);
            else if(users.length == 0)
                callback(null, false);
            else callback(null, true);
        });
    },
    createUser: function (username, publickey, callback) {
        (new models.User({ userName: username, publicKey: publickey })).save(function(err, user) {
            if(err) callback(err, null);
            else callback(null, user);
        });
    },
    close: function(callback) {
        mongoose.disconnect(callback);
    }
};