module.exports = exports = {
    splitUrl: function splitUrl(url) {
        var urlspl      = url.split(/^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/);
        var path        = urlspl[5].split('/');
        if(urlspl[7])
            var query   = urlspl[7].split('&');
        
        return {
            path: path,
            query: query,
            split: urlspl
        };
    },
    buildMongoString: function buildMongoString (host, port, database, username, password) {
        if(typeof port == "string") {
            database = port;
            username = database;
            password = username;
            port = 27017;
        }
        var str = "mongodb://";
        if(username && password) {
            str += username + ":" + password;
        } else if(username || password) throw "You need to define either both username and password or none!";
        str += "@";
        if(host) str += host; else throw "You need to define the host!";
        str += ":";
        if(port) str += port; else throw "No port defined!";
        str += "/";
        if(database) str += database; else throw "No database defined!";

        return str;
    },
    jsonHasOwnProperties: function jsonHasOwnProperties(json, properties) {
        if(!json)
            throw "You need to define a JSON object!";
        if(json.constructor !== Object)
            throw "JSON has to be an object!";
        if(!properties)
            throw "You have to define properties!";
        if(properties.constructor !== Array)
            throw "Properties need to be an array!";

        for(var i = 0; i < properties.length; i++) {
            if(!json.hasOwnProperty(properties[i])) return false;
        }

        return true;
    },
    jsonAtLeastHasOneProperty: function jsonAtLeastHasOneProperty(json, properties) {
        if(!json)
            throw "You need to define a JSON object!";
        if(json.constructor !== Object)
            throw "JSON has to be an object!";
        if(!properties)
            throw "You have to define properties!";
        if(properties.constructor !== Array)
            throw "Properties need to be an array!";

        for(var i = 0; i < properties.length; i++) {
            if(json.hasOwnProperty(properties[i])) return true;
        }

        return false;
    }
};