#!/usr/bin/node
var config      = require('./config.js');
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var filter      = require('content-filter');
var sass        = require('node-sass-middleware');
var paths       = require('require-all')(__dirname + '/models');
var _           = require('./lib/functions');

app.set('view engine', 'pug');
app.set('views', __dirname + '/templates/views');

app.use(sass({
    src: __dirname + '/templates/css',
    dest: __dirname + '/static/css',
    outputStyle: 'compressed',
    prefix: '/css'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(filter());

app.use('/css', express.static(__dirname + '/static/css'));
app.use('/fonts', express.static(__dirname + '/static/fonts'));
app.use('/img', express.static(__dirname + '/static/img'));
app.use('/js', express.static(__dirname + '/static/js'));

app.get('*', function(req, res) {
    var path      = _.splitUrl(req.url).path;

    if(path[1] == 'index') paths.error(req, res, 404);
    else if(path[1].length == 0) paths.index.get(req, res);
    else {
        if(paths.hasOwnProperty(path[1])) {
            if(paths[path[1]].hasOwnProperty('get')) {
                paths[path[1]].get(req, res);
            } else paths.error(req, res, 404);
        } else paths.error(req, res, 404);
    }
});

app.post('*', function(req, res) {
    var path        = _.splitUrl(req.url).path;

    if(path[1] == 'index') paths.error(req, res, 404);
    else if(path[1].length == 0) paths.index.get(req, res);
    else {
        if(paths.hasOwnProperty(path[1])) {
            if(paths[path[1]].hasOwnProperty('post')) {
                paths[path[1]].post(req, res);
            }
            else paths.error(req, res, 404);
        } else paths.error(req, res, 404);
    }
});

app.listen(config.port | 5873);