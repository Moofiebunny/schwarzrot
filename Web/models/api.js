var _       = require("../lib/functions");
var db      = require("../lib/database");
var fs      = require("fs");
var pug     = require("pug");

var err = {
    BAD_REQUEST:    {result: null, error: "bad_request"},
    NOT_FOUND:      {result: null, error: "not_found"}
};

module.exports = exports = {
    post: function(req, res) {
        var path   = _.splitUrl(req.url).path;
        var api    = path[2];

        if(api && api.length > 0) {
            switch(api) {
                case "register":
                    if(path[3] && path[3] == "checkfields") {
                        if(!_.jsonAtLeastHasOneProperty(req.body, [ "invitecode", "username" ]))
                            res.json(err.BAD_REQUEST);
                        else {
                            var response = {};
                            var callbacks = {
                                invitecode: function() { callbacks.username(); },
                                username: function() { res.json(response); }
                            };

                            if(req.body.invitecode)
                                callbacks.invitecode = function() {
                                    db.checkInviteCode(req.body.invitecode, function(err, working) {
                                        if(err) {
                                            console.log(err);
                                            response.invitecode = false;
                                        } else {
                                            response.invitecode = working;
                                        }

                                        callbacks.username();
                                    });
                                };

                            if(req.body.username)
                                callbacks.username = function() {
                                    db.checkIfUsernameExists(req.body.username, function(err, exists) {
                                        if(err) {
                                            console.log(err);
                                            response.username = false;
                                        } else {
                                            response.username = !exists;
                                        }

                                        res.json(response);
                                    });
                                };

                            callbacks.invitecode();
                        }
                    } else res.json(err.BAD_REQUEST);
                    break;
            }
        } else res.json(err.BAD_REQUEST);

    },
    get: function(req, res) {
    }
};