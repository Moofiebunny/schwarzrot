module.exports = exports = function(req, res, code) {
    res.status(code).send('<h1>Error: %1</h1>'.replace('%1', code));
};